FROM alpine/curl
WORKDIR /tmp
RUN set -x && \
    curl https://github.com/kdheepak/taskwarrior-tui/releases/download/v0.23.7/taskwarrior-tui.deb -LO  && \
    ls -lhart

FROM ubuntu

WORKDIR /root/
COPY --from=0 /tmp/taskwarrior-tui.deb ./
COPY taskd.sh /opt/taskd.sh

# Remove SUID programs
RUN for i in `find / -perm +6000 -type f 2>/dev/null`; do chmod a-s $i; done

# Taskd user, volume and port, logs
RUN addgroup --gid 53589 taskd && \
    adduser --uid 53589 --gid 53589 --disabled-password --gecos "taskd" taskd && \
    usermod -L taskd && \
    mkdir -p /var/taskd && \
    chmod 700 /var/taskd /opt/taskd.sh && \
    ln -sf /dev/stdout /var/log/taskd.log && \
    chown taskd:taskd /var/taskd /var/log/taskd.log /opt/taskd.sh
VOLUME /var/taskd
EXPOSE 53589

# Fetch taskd and dependencies, build and install taskd, remove build chain and source files
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git ca-certificates build-essential cmake gnutls-bin libgnutls28-dev uuid-dev && \
    git clone --recursive -b 1.2.0 https://github.com/GothenburgBitFactory/taskserver.git /opt/taskd && \
    cd /opt/taskd && \
    cmake . && \
    make && \
    make install && \
    rm -rf /opt/taskd && \
    DEBIAN_FRONTEND=noninteractive apt-get remove -y --auto-remove git build-essential cmake && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get --yes update && \
    apt-get --yes install neovim python3 python3-pip && \
    apt --yes install ./taskwarrior-tui.deb && apt-get clean && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    rm -f /root/taskwarrior-tui.deb && \
    pip3 install taskwarrior-syncall[google]

USER taskd
CMD /opt/taskd.sh
